(use-modules (gnu)
	     (gnu system nss)
	     (guix utils)
	     (guix packages)
	     (guix gexp)
	     (nongnu packages linux)
	     (nongnu system linux-initrd))
(use-service-modules desktop xorg pm)
(use-package-modules certs gnome emacs emacs-xyz shells file-systems)

(define my-kernel linux)
(define my-zfs
  (package
   (inherit zfs)
   (arguments (cons* #:linux my-kernel
                     (package-arguments zfs)))))

(operating-system
  (host-name "thinkpad")
  (timezone "US/Eastern")
  (locale "en_US.utf8")
  (keyboard-layout (keyboard-layout "us" "colemak" #:options '("ctrl:nocaps")))

  ;; nongnu
  (kernel my-kernel)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  ; ZFS stuff
  (kernel-loadable-modules (list (list my-zfs "module")))
  (kernel-arguments (cons* "libata.force=noncq" %default-kernel-arguments))
   
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)))

  (file-systems (append
                 (list (file-system
                         (device (uuid "bcc05365-8881-4d0e-8c6f-493699cf881e"))
                         (mount-point "/")
                         (type "btrfs"))
                       (file-system
                         (device (uuid "0021-DE06" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat")))
                 %base-file-systems))

  (users (cons (user-account
                (name "shcv")
		(comment "Samuel Christie")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
		(shell #~(string-append #$zsh "/bin/zsh")))
               %base-user-accounts))

  (packages (append (list
                     nss-certs
		     emacs
		     emacs-exwm
		     emacs-desktop-environment
		     zsh)
		    (list my-zfs)
                    %base-packages))

  (services (append (list (service gnome-desktop-service-type)
                          (service xfce-desktop-service-type)
                          (set-xorg-configuration
                           (xorg-configuration
                            (keyboard-layout keyboard-layout)))
			  (service tlp-service-type
				   (tlp-configuration
				    (cpu-scaling-governor-on-ac (list "performance"))
				    (sched-powersave-on-bat? #t)))
			  (service thermald-service-type))
                    (modify-services %desktop-services
		      (guix-service-type
		       config =>
		       (guix-configuration
			(inherit config)
			(substitute-urls
			 (append (list "https://substitutes.nonguix.org")
				 %default-substitute-urls))
			(authorized-keys
			 (append (list (local-file "./nonguix-signing-key.pub"))
				 %default-authorized-guix-keys)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
