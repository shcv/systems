(use-modules (guix packages)
             (guix download)
             (guix gexp)
             (guix git-download)
             (guix build-system trivial)
             (gnu)
             (gnu system nss)
	     (gnu services)
	     (nongnu packages linux)
             (nongnu system linux-initrd))
(use-service-modules avahi base dbus desktop networking sddm xorg ssh vpn linux shepherd)
(use-package-modules admin certs linux shells file-systems)

(define my-kernel linux)
(define my-zfs
  (package
   (inherit zfs)
   (arguments (cons* #:linux my-kernel
                     (package-arguments zfs)))))

(define zfs-shepherd-services
  (let ((zpool            (file-append my-zfs "/sbin/zpool"))
        (zfs              (file-append my-zfs "/sbin/zfs"))
        (scheme-modules   `((srfi srfi-1)
                            (srfi srfi-34)
                            (srfi srfi-35)
                            (rnrs io ports)
                            ,@%default-modules)))
    (define zfs-scan 
      (shepherd-service
        (provision '(zfs-scan))
        (documentation "Scans for ZFS pools.")
        (requirement '(kernel-module-loader udev))
        (modules scheme-modules)
        (start #~(lambda _
                   (invoke/quiet #$zpool "import" "-a" "-N")))
        (stop #~(const #f))))
    (define zfs-automount
      (shepherd-service
        (provision '(zfs-automount))
        (documentation "Automounts ZFS data sets.")
        (requirement '(zfs-scan))
        (modules scheme-modules)
        (start #~(lambda _
                   (with-output-to-port
                     (current-error-port)
                     (lambda ()
                       (invoke #$zfs "mount" "-a" "-l")))))
        (stop #~(lambda _
                  (chdir "/") 
                  (invoke/quiet #$zfs "unmount" "-a" "-f")
                  #f))))
    (list zfs-scan   
          zfs-automount)))

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout
   (keyboard-layout "us" "colemak" #:options '("ctrl:nocaps")))
  (host-name "terminus")

  ; nonguix kernel, initrd, and firmware
  (kernel my-kernel)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  (kernel-loadable-modules (list (list my-zfs "module")))
 
  (users (cons* (user-account
                 (name "shcv")
                 (uid 1000)
                 (comment "Samuel Christie")
                 (group "users")
                 (home-directory "/home/shcv")
                 (supplementary-groups
                  '("wheel" "netdev" "audio" "video"))
                 (shell #~(string-append #$zsh "/bin/zsh")))
                %base-user-accounts))
  (packages
    (append
     (map specification->package
          (list "emacs"
                "emacs-exwm"
                "emacs-desktop-environment"
                "nss-certs"
                "zsh"))
     (list my-zfs)
     %base-packages))
  (services
    (append
     (list (service network-manager-service-type)
	   (service modem-manager-service-type)
	   (service wpa-supplicant-service-type)
	   (service elogind-service-type)
           (service openssh-service-type
                    (openssh-configuration
                     (permit-root-login 'prohibit-password)))
           (service tor-service-type)
           (set-xorg-configuration
            (xorg-configuration
             (keyboard-layout keyboard-layout)))
           (service wireguard-service-type
                    (wireguard-configuration
                     (addresses '("10.42.0.2/32"))
                     (peers
                      (list
                       (wireguard-peer
                        (name "skyhook")
                        (endpoint "104.238.179.57:51820")
                        (public-key "1QEBmLdGo++JENF4wvH4t/CzN6bxmHXzrA4Pixbmq3I=")
                        (allowed-ips '("10.42.0.0/16")))))))
	   (simple-service 'my-zfs-loader
                           kernel-module-loader-service-type
                           '("zfs"))
	   (simple-service 'zfs-shepherd-services
                           shepherd-root-service-type
                           zfs-shepherd-services)
           (simple-service 'zfs-sheperd-services-user-processes
                           user-processes-service-type
                           '(zfs-automount)))
      %base-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "7ecb6e13-be63-4905-a4b4-2fb3ac933555"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "8CFC-6751" 'fat32))
             (type "vfat"))
           %base-file-systems)))
