(use-modules (guix packages)
             (guix download)
             (guix gexp)
             (guix git-download)
             (guix build-system trivial)
             (gnu)
             (gnu system nss)
	     (gnu services)
	     (nongnu packages linux)
             (nongnu system linux-initrd)
	     (services samba))
(use-service-modules avahi base dbus desktop networking sddm xorg ssh vpn linux shepherd docker)
(use-package-modules admin certs linux shells file-systems)

(define my-kernel linux)
(define my-zfs
  (package
   (inherit zfs)
   (arguments (cons* #:linux my-kernel
                     (package-arguments zfs)))))

(define zfs-shepherd-services
  (let ((zpool            (file-append my-zfs "/sbin/zpool"))
        (zfs              (file-append my-zfs "/sbin/zfs"))
        (scheme-modules   `((srfi srfi-1)
                            (srfi srfi-34)
                            (srfi srfi-35)
                            (rnrs io ports)
                            ,@%default-modules)))
    (define zfs-scan 
      (shepherd-service
        (provision '(zfs-scan))
        (documentation "Scans for ZFS pools.")
        (requirement '(kernel-module-loader udev))
        (modules scheme-modules)
        (start #~(lambda _
                   (invoke/quiet #$zpool "import" "-a" "-N")))
        (stop #~(const #f))))
    (define zfs-automount
      (shepherd-service
        (provision '(zfs-automount))
        (documentation "Automounts ZFS data sets.")
        (requirement '(zfs-scan))
        (modules scheme-modules)
        (start #~(lambda _
                   (with-output-to-port
                     (current-error-port)
                     (lambda ()
                       (invoke #$zfs "mount" "-a" "-l")))))
        (stop #~(lambda _
                  (chdir "/") 
                  (invoke/quiet #$zfs "unmount" "-a" "-f")
                  #f))))
    (list zfs-scan   
          zfs-automount)))

(define (user name)
  (user-account
   (name name)
   (group "users")
   (supplementary-groups
    '("wheel" "netdev" "audio" "video" "samba"))))

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout
   (keyboard-layout "us" #:options '("ctrl:nocaps")))
  (host-name "sandbox")

  ; nonguix kernel, initrd, and firmware
  (kernel my-kernel)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  ; ZFS stuff
  (kernel-loadable-modules (list (list my-zfs "module")))
  
  (groups (cons* (user-group (name "samba"))
		 %base-groups))
  (users (cons* (user-account
                 (name "shcv")
                 (uid 1000)
                 (group "users")
                 (home-directory "/home/shcv")
                 (supplementary-groups
                  '("wheel" "netdev" "audio" "video" "samba"))
                 (shell #~(string-append #$zsh "/bin/zsh")))
                (user "sam")
                (user "stephen")
                (user "martha")
                (user "samba") ; samba guest account
                %base-user-accounts))
  (packages
    (append
     (map specification->package
          (list "emacs"
                "emacs-exwm"
                "emacs-desktop-environment"
                "nss-certs"
                "zile"
                "zsh"))
     (list my-zfs)
     %base-packages))
  (services
    (append
     (list (service network-manager-service-type)
           (service modem-manager-service-type)
           (service wpa-supplicant-service-type)
           (service ntp-service-type)
           (service docker-service-type)
           (service elogind-service-type)
           (service openssh-service-type
                    (openssh-configuration
                     (permit-root-login 'prohibit-password)))
           (service samba-service-type
		    (samba-configuration
                     (hosts-allow '("127.0.0.1" "192.168.1.0/24"))
                     (hosts-deny '("0.0.0.0/0"))
                     (guest-account "samba")
                     (map-to-guest "Bad user")
                     (shares
                      (list (samba-share-configuration
                             (name "homes")
                             (writable? #t)
                             (browsable? #f))
                            (samba-share-configuration
                             (name "Backups")
                             (comment "Backups")
                             (path "/data/backups")
                             (writable? #t)
                             (guest-ok? #t)
                             (write-list '("shcv" "sam" "martha" "stephen" "@samba")))
                            (samba-share-configuration
                             (name "Photos")
                             (comment "Photos")
                             (path "/data/photos")
                             (writable? #t)
                             (guest-ok? #t)
                             (write-list '("shcv" "sam" "martha" "stephen" "@samba")))
                            (samba-share-configuration
                             (name "Music")
                             (comment "Music")
                             (path "/data/music")
                             (writable? #t)
                             (guest-ok? #t)
                             (write-list '("shcv" "sam" "martha" "stephen" "@samba")))
                            (samba-share-configuration
                             (name "Games")
                             (comment "Games")
                             (path "/data/games")
                             (writable? #t)
                             (guest-ok? #t)
                             (write-list '("shcv" "sam" "martha" "stephen" "@samba")))
                            (samba-share-configuration
                             (name "Business")
                             (comment "Business")
                             (path "/data/business")
                             (writable? #t)
                             (write-list '("shcv" "sam" "martha" "stephen" "@samba")))))))
           (simple-service 'my-zfs-loader
                           kernel-module-loader-service-type
                           '("zfs"))
           (simple-service 'zfs-shepherd-services
                           shepherd-root-service-type
                           zfs-shepherd-services)
           (simple-service 'zfs-sheperd-services-user-processes
                           user-processes-service-type
                           '(zfs-automount)))
      %base-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "5824b846-4201-44c0-ae60-c5f6c25d5aff"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "AAC6-F58D" 'fat32))
             (type "vfat"))
           %base-file-systems)))
