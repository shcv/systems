;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
             (gnu packages linux)
             (nongnu packages linux)
             (nongnu system linux-initrd))
(use-package-modules shells emacs emacs-xyz certs linux audio haskell-apps)
(use-service-modules cups desktop networking ssh xorg pm docker audio sound dbus)

(define my-kernel
  ;; kernel configs to support the MediaTek MT7922 in an Asus Zenbook S 13
  (customize-linux
   #:linux linux
   #:configs '("CONFIG_MT76_CORE=m"
               "CONFIG_MT76_LEDS=y"
               "CONFIG_MT76_CONNAC_LIB=m"
               "CONFIG_MT7921_COMMON=m"
               "CONFIG_MT7921E=m"
               "CONFIG_WLAN=y"
               "CONFIG_WLAN_VENDOR_MEDIATEK=y")))

(define %xorg-libinput-configuration
  "Section \"InputClass\"
    Identifier \"Touchpads\"
    Driver \"libinput\"
    MatchDevicePath \"/dev/input/event*\"
    MatchIsTouchpad \"on\"

    Option \"Tapping\" \"on\"
    Option \"TappingDrag\" \"on\"
    Option \"DisableWhileTyping\" \"on\"
    Option \"MiddleEmulation\" \"on\"
    Option \"ScrollMethod\" \"twofinger\"
EndSection")

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us" #:options '("ctrl:nocaps")))
  (host-name "zenbook")

  (kernel my-kernel)
  (firmware (list linux-firmware))
  (initrd microcode-initrd)

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                 (name "shcv")
                 (comment "Samuel Christie")
                 (uid 1000)
                 (group "users")
                 (home-directory "/home/shcv")
                 (shell #~(string-append #$zsh "/bin/zsh"))
                 (supplementary-groups '("wheel" "netdev" "audio" "video" "docker" "lp" "input")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (list
                     bluez
                     bluez-alsa
                     emacs
                     emacs-desktop-environment
                     emacs-exwm
                     kmonad
                     le-certs)
                    %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list
            ;; To configure OpenSSH, pass an 'openssh-configuration'
            ;; record as a second argument to 'service' below.
            (service openssh-service-type)
            (set-xorg-configuration
             (xorg-configuration
              (keyboard-layout keyboard-layout)
              (extra-config (list %xorg-libinput-configuration))))
            (service tlp-service-type
                     (tlp-configuration
                      (cpu-scaling-governor-on-ac (list "performance"))
                      (sched-powersave-on-bat? #t)))
            (service thermald-service-type)
            (service containerd-service-type)
            (service docker-service-type)
            (service bluetooth-service-type))
           (modify-services %desktop-services
             (guix-service-type config =>
                                (guix-configuration
                                 (inherit config)
                                 (substitute-urls
                                  (append (list "https://substitutes.nonguix.org")
                                          %default-substitute-urls))
                                 (authorized-keys
                                  (append (list (local-file "./nonguix-signing-key.pub"))
                                          %default-authorized-guix-keys))))
             (udev-service-type config =>
                                (udev-configuration (inherit config)
                                 (rules (cons kmonad
                                              (udev-configuration-rules config))))))))

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/boot/efi"))
               (keyboard-layout keyboard-layout)))

  (swap-devices (list (swap-space
                       (target (uuid "fbecb5b0-4061-4f6b-8ada-393523ee3af3")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                          (mount-point "/boot/efi")
                          (device (uuid "B620-3AE5"
                                          'fat32))
                          (type "vfat"))
                       (file-system
                          (mount-point "/")
                          (device (uuid
                                     "d6df68fc-72d7-4a7c-b0fa-1c6c3f86c4d3"
                                     'ext4))
                          (type "ext4"))
                       %base-file-systems)))
