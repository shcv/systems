;; Adapted from the nonguix install.scm
;; Generate a bootable image (e.g. for USB sticks, etc.) with:
;; $ guix system disk-image install.scm


(define-module (nongnu system install)
  #:use-module (guix packages)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages zile)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu system)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system install)
  #:use-module (gnu packages linux)
  #:use-module (nongnu packages linux)
  #:export (installation-os-nonfree))

(define my-kernel
  ;; kernel configs to support the MediaTek MT7922 in an Asus Zenbook S 13
  (customize-linux
   #:linux linux
   #:configs '("CONFIG_MT76_CORE=m"
               "CONFIG_MT76_LEDS=y"
               "CONFIG_MT76_CONNAC_LIB=m"
               "CONFIG_MT7921_COMMON=m"
               "CONFIG_MT7921E=m"
               "CONFIG_WLAN=y"
               "CONFIG_WLAN_VENDOR_MEDIATEK=y")))

(define my-zfs
  (package
   (inherit zfs)
   (arguments (cons* #:linux my-kernel
                     (package-arguments zfs)))))

(define installation-os-nonfree
  (operating-system
   (inherit installation-os)
   (kernel my-kernel)
   (firmware (list linux-firmware))
   (keyboard-layout
    (keyboard-layout "us" "colemak" #:options '("ctrl:nocaps")))

    ; ZFS stuff
   (kernel-loadable-modules (list (list my-zfs "module")))
   (kernel-arguments (cons* "libata.force=noncq" %default-kernel-arguments))

   (packages
    (append
      (list curl
            git
            emacs
            zile
            my-zfs)
      (operating-system-packages installation-os)))))

installation-os-nonfree
