;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Simon Streit <simon@netpanic.org>
;;; Copyright © 2022 Samuel Christie <shcv@sdf.org>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (services samba)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services base)
  #:use-module (gnu system shadow)

  #:use-module (gnu packages admin)
  #:use-module (gnu packages samba)

  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix modules)
  #:use-module (guix records)

  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)
  #:use-module (system syntax)
  #:export (samba-service-type

            samba-configuration
            samba-configuration?
            samba-configuration-package
            samba-configuration-config-file
            samba-configuration-enable-samba?
            samba-configuration-enable-smbd?
            samba-configuration-enable-nmbd?
            samba-configuration-enable-winbindd?
            samba-configuration-global-extra-config
            samba-configuration-workgroup
            samba-configuration-server-string
            samba-configuration-server-role
            samba-configuration-bind-interfaces-only?
            samba-configuration-interfaces
            samba-configuration-hosts-allow
            samba-configuration-hosts-deny
            samba-configuration-guest-account
            samba-configuration-map-to-guest
            samba-configuration-log-file
            samba-configuration-logging
            samba-configuration-realm
            samba-configuration-passdb-backend
            samba-configuration-include-config
            samba-configuration-logon-path
            samba-configuration-wins-support?
            samba-configuration-wins-server
            samba-configuration-wins-proxy?
            samba-configuration-dns-proxy?
            samba-configuration-shares

            samba-share-configuration
            samba-share-configuration?
            samba-share-configuration-name
            samba-share-configuration-path
            samba-share-configuration-comment
            samba-share-configuraiton-guest-ok?
            samba-share-configuration-writable?
            samba-share-configuration-browsable?
            samba-share-configuration-force-user
            samba-share-configuration-write-list
            ))

;;; Commentary:
;;;
;;; Windows network services.
;;;
;;; Code:

;;; Utility helpers - should be moved to a shared lib
(define-syntax-parameter <> (syntax-rules ()))
(define-syntax-rule (and/l x tail ...)
  (let ((x* x))
    (if x*
        (syntax-parameterize ((<> (identifier-syntax x*)))
          (list tail ...))
        '())))

(define (flatten . lst)
  "Return a list that recursively concatenates all sub-lists of LST."
  (define (flatten1 head out)
    (if (list? head)
        (fold-right flatten1 out head)
        (cons head out)))
  (fold-right flatten1 '() lst))

;;; Record Definitions

(define-record-type* <samba-share-configuration>
  samba-share-configuration
  make-samba-share-configuration
  samba-share-configuration?

  (name       samba-share-configuration-name) ; string
  (path       samba-share-configuration-path  ; string
              (default #f))
  (writable?  samba-share-configuration-writable? ; bool
              (default #f))
  (browsable? samba-share-configuration-browsable? ; bool
              (default #t))
  (comment    samba-share-configuration-comment ; string
              (default #f))
  (guest-ok?  samba-share-configuraiton-guest-ok? ; bool
              (default #f))
  (force-user samba-share-configuration-force-user ; string
              (default #f))
  (write-list samba-share-configuration-write-list ; list of string user names
              (default '()))
  )

(define-record-type* <samba-configuration>
  samba-configuration
  make-samba-configuration
  samba-configuration?

  (package              samba-configuration-package
                        (default samba))
  (config-file          samba-configuration-config-file
                        (default #f))
  (enable-samba?        samba-configuration-enable-samba?
                        (default #f))
  (enable-smbd?         samba-configuration-enable-smbd?
                        (default #t))
  (enable-nmbd?         samba-configuration-enable-nmbd?
                        (default #t))
  (enable-winbindd?     samba-configuration-enable-winbindd?
                        (default #f))

  ;; From here on anything goes to smb.conf

  ;; This line will be put at the end of [global].
  (global-extra-config   samba-configuration-global-extra-config
                         (default #f))
  (workgroup             samba-configuration-workgroup
                         (default "WORKGROUP"))
  (server-string         samba-configuration-server-string
                         (default "Samba Server %v"))
  (server-role           samba-configuration-server-role
                         (default "standalone server"))
  (bind-interfaces-only? samba-configuration-bind-interfaces-only?
                         (default #f))
  (interfaces            samba-configuration-interfaces
                         (default '()))
  (hosts-allow           samba-configuration-hosts-allow
                         (default '()))
  (hosts-deny            samba-configuration-hosts-deny
                         (default '()))
  (guest-account         samba-configuration-guest-account
                         (default #f))
  (map-to-guest          samba-configuration-map-to-guest
                         (default #f))
  (log-file              samba-configuration-log-file
                         (default "/var/log/samba/log.%m"))
  (logging               samba-configuration-logging
                         (default "file"))
  (realm                 samba-configuration-realm
                         (default #f))
  (passdb-backend        samba-configuration-passdb-backend
                         (default #f))
  (include-config        samba-configuration-include-config
                         (default #f))
  (logon-path            samba-configuration-logon-path
                         (default #f))
  (wins-support?         samba-configuration-wins-support?
                         (default #f))
  (wins-server           samba-configuration-wins-server
                         (default #f))
  (wins-proxy?           samba-configuration-wins-proxy?
                         (default #f))
  (dns-proxy?            samba-configuration-dns-proxy?
                         (default #f))
  (shares                samba-configuration-shares ; list of <samba-share-configuration>
                         (default '()))
  )

;;; Configuration Generation Functions

(define (emit-samba-share-config share)
  (match-record share <samba-share-configuration>
                (name path writable? browsable? comment guest-ok? force-user write-list)
    (flatten
     (list "\n[" name "]\n")
     (and/l path "    path = " <> "\n")
     (if (not browsable?) "    browsable = no\n" '())
     (if writable? "    writable = yes\n" '())
     (and/l comment "    comment = " <> "\n")
     (and/l guest-ok? "    guest ok = yes\n")
     (if (not (nil? write-list))
         (list "    write list = " (string-join write-list ", ") "\n")
         '())
     )))

(define (emit-samba-config config)
  (match-record config <samba-configuration>
                (workgroup server-string server-role bind-interfaces-only?
                 interfaces hosts-allow hosts-deny guest-account
                 map-to-guest log-file logging realm passdb-backend
                 include-config logon-path wins-support? wins-server
                 wins-proxy? dns-proxy? global-extra-config shares)
    (flatten
     "# Generated by samba-service.
[global]
"
     (and/l workgroup "    workgroup = " <> "\n")
     (and/l server-string "    server string = " <> "\n")
     (and/l server-role "    server role = " <> "\n")
     (and/l bind-interfaces-only? "    bind interfaces only = yes\n")
     (if (not (nil? interfaces))
         (list "    interfaces = " (string-join interfaces) "\n")
         "")
     (if (not (nil? hosts-allow))
         (list "    hosts allow = " (string-join hosts-allow) "\n")
         "")
     (if (not (nil? hosts-deny))
         (list "    hosts deny = " (string-join hosts-deny) "\n")
         "")
     (and/l guest-account "    guest account = " <> "\n")
     (and/l map-to-guest "    map to guest = " <> "\n")
     (and/l log-file "    log file = " <> "\n")
     (and/l logging "    logging = " <> "\n")
     (and/l realm "    realm = " <> "\n")
     (and/l passdb-backend config "    passdb backend = " <> "\n")
     (and/l include-config "    include config = " <> "\n")
     (and/l logon-path "    logon path = " <> "\n")
     (and/l wins-support? "    wins support = yes\n")
     (and/l wins-server "    wins server = " <> "\n")
     (and/l wins-proxy? "    wins proxy = yes\n")
     (and/l dns-proxy? "    dns proxy = yes\n")
     (and/l global-extra-config
            "\n#Extra options provided by ‘global-extra-config’:\n" <> "\n")
     (if (not (nil? shares))
         (map emit-samba-share-config shares)
         '()))))

(define (default-samba-config config)
  (apply mixed-text-file "smb.conf"
         (emit-samba-config config)))

(define (samba-activation config)
  (match-record config <samba-configuration>
                (config-file)
    (with-imported-modules '((guix build utils))
      (let ((config-file
             (or config-file
                 (default-samba-config config)))
            (lib-directory "/var/lib/samba")
            (log-directory "/var/log/samba")
            (run-directory "/var/run/samba")
            (smb.conf "/etc/samba/smb.conf"))
        #~(begin
            (use-modules (guix build utils))
            (mkdir-p #$log-directory)
            (mkdir-p #$run-directory)
            (mkdir-p (string-append #$lib-directory "/private"))
            (mkdir-p "/etc/samba")
            (copy-file #$config-file #$smb.conf)

            ;; Test config
            (system* (string-append #$samba "/bin/testparm")
                     "--suppress-prompt")
            )))))

(define (samba-shepherd-service config)
  (match-record config <samba-configuration>
    (package)
    (let ((config-file "/etc/samba/smb.conf"))
      (list (shepherd-service
             (documentation "Run the Samba")
             (provision '(samba))
             (requirement '(networking))
             (start #~(make-forkexec-constructor
                       (list #$(file-append samba "/sbin/samba")
                             (string-append "--configfile="
                                            #$config-file)
                             "--foreground"
                             "--no-process-group")))
             (stop #~(make-kill-destructor)))))))

(define (samba-nmbd-shepherd-service config)
  (match-record config <samba-configuration>
    (package)
    (let ((config-file "/etc/samba/smb.conf"))
      (list (shepherd-service
             (documentation "Run NetBIOS name server.")
             (provision '(samba-nmbd))
             (requirement '(networking))
             (start #~(make-forkexec-constructor
                       (list #$(file-append samba "/sbin/nmbd")
                             (string-append "--configfile="
                                            #$config-file)
                             "--foreground"
                             "--no-process-group")))
             (stop #~(make-kill-destructor)))))))

(define (samba-smbd-shepherd-service config)
  (match-record config <samba-configuration>
    (package)
    (let ((config-file "/etc/samba/smb.conf"))
      (list (shepherd-service
             (documentation "Run SMB/CIFS service")
             (provision '(samba-smbd))
             (requirement '(networking))
             (start #~(make-forkexec-constructor
                       (list #$(file-append samba "/sbin/smbd")
                             (string-append "--configfile="
                                            #$config-file)
                             "--foreground"
                             "--no-process-group")))
             (stop #~(make-kill-destructor)))))))

(define (samba-winbind-shepherd-service config)
  (match-record config <samba-configuration>
    (package)
    (let ((config-file "/etc/samba/smb.conf"))
      (list (shepherd-service
             (documentation "Run winbindd for Name Service Switch")
             (provision '(samba-winbindd))
             (requirement '(networking))
             (start #~(make-forkexec-constructor
                       (list #$(file-append samba "/sbin/winbindd")
                             (string-append "--configfile="
                                            #$config-file)
                             "--foreground"
                             "--no-process-group")))
             (stop #~(make-kill-destructor)))))))

(define (samba-shepherd-services config)
  (append ;; (samba-shepherd-service config)
             (samba-nmbd-shepherd-service config)
             (samba-smbd-shepherd-service config)
             (samba-winbind-shepherd-service config)))

(define samba-service-type
  (service-type
   (name 'samba)
   (description "Samba")
   (extensions
    (list (service-extension shepherd-root-service-type
                             samba-shepherd-services)
          (service-extension activation-service-type
                             samba-activation)
          ))
   (default-value (samba-configuration))))
