;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 display server.

(use-modules (gnu)
             (guix gexp))
(use-service-modules networking ssh web certbot dbus docker desktop vpn telephony mail)
(use-package-modules ssh tmux shells tls certs linux mail)

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define hnst-cert
  "/etc/letsencrypt/live/hnst.sh/fullchain.pem")
(define hnst-key
  "/etc/letsencrypt/live/hnst.sh/privkey.pem")

(define wkd-cert
  "/etc/letsencrypt/live/wkd/fullchain.pem")
(define wkd-key
  "/etc/letsencrypt/live/wkd/privkey.pem")


(define virtual-mail-domains
  (plain-file "virtual-mail-domains" "
hnst.sh
*.hnst.sh
hackandstash.com
*.hackandstash.com
"))

(define virtual-mail-users
  (plain-file "virtual-mail-users" "
shcv@hnst.sh shcv
shcv@hackandstash.com shcv
postmaster@hnst.sh shcv
postmaster@hackandstash.com shcv
logger@hnst.sh |/run/current-system/profile/bin/logger -t mail
"))

(define mail-aliases
  (plain-file "aliases" "
shcv: shcv
root: shcv
postmaster: shcv
abuse: shcv"))

(operating-system
  (host-name "skyhook")
  (timezone "US/Eastern")
  (locale "en_US.utf8")
  (keyboard-layout (keyboard-layout "us"))


  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets (list "/dev/vda"))
               (keyboard-layout keyboard-layout)))

  (swap-devices (list (swap-space
                       (target (uuid
                                "d1389fb8-d5b7-42c0-a496-e45ca937578c")))))

  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "06e1a9db-eacf-42ed-ad15-a01314babeda"
                                  'ext4))
                         (type "ext4"))
                       %base-file-systems))

  (users (cons* (user-account
                 (name "shcv")
                 (uid 1000)
                 (group "users")
                 (supplementary-groups '("wheel" "netdev" "docker"))
                 (home-directory "/home/shcv")
                 (shell #~(string-append #$zsh "/bin/zsh")))
                (user-account
                 (name "spc")
                 (group "users")
                 (supplementary-groups '("wheel" "docker"))
                 (home-directory "/home/spc"))
                (user-account
                 (system? #true)
                 (name "_dkimsign")
                 (group "_dkimsign")
                 (home-directory "/etc/mail/dkim"))
                %base-user-accounts))

  (groups
   (append (list
            (user-group
             (system? #true)
             (name "_dkimsign")))
           %base-groups))

  (packages (cons*
             mosh tmux nss-certs iptables
             %base-packages))

  (services (cons* (service dhcp-client-service-type)
                   (service dbus-root-service-type)
                   (service elogind-service-type)
                   (service docker-service-type)
                   (service openssh-service-type
                            (openssh-configuration
                             (port-number 2222)))
                   (service ntp-service-type)
                   (service nginx-service-type
                            (nginx-configuration
                             (server-blocks
                              (list (nginx-server-configuration
                                     (server-name '("hackandstash.com"))
                                     (listen '("443 ssl"))
                                     (root "/srv/http/hackandstash.com")
                                     (ssl-certificate "/etc/letsencrypt/live/hackandstash.com/fullchain.pem")
                                     (ssl-certificate-key "/etc/letsencrypt/live/hackandstash.com/privkey.pem")
                                     (locations (list (nginx-location-configuration
                                                       (uri "~ ^/~(.+?)(/.*)?$")
                                                       (body (list "alias /home/$1/public/www$2;"
                                                                   "index index.html;"
                                                                   "autoindex on;"))))))
                                    (nginx-server-configuration
                                     (server-name '("hnst.sh"))
                                     (listen '("443 ssl"))
                                     (root "/srv/http/hnst.sh")
                                     (ssl-certificate hnst-cert)
                                     (ssl-certificate-key hnst-key)
                                     (locations (list (nginx-location-configuration
                                                       (uri "~ ^/~(.+?)(/.*)?$")
                                                       (body (list "alias /home/$1/public/www$2;"
                                                                   "index index.html;"
                                                                   "autoindex on;"))))))
                                    (nginx-server-configuration
                                     (server-name '("openpgpkey.hnst.sh" "openpgpkey.hyperlisp.net" "openpgpkey.hackandstash.com"))
                                     (listen '("443 ssl"))
                                     (root "/srv/wkd")
                                     (ssl-certificate wkd-cert)
                                     (ssl-certificate-key wkd-key)
                                     (locations (list (nginx-location-configuration
                                                       (uri "~ /.well-known/openpgpkey/(.+)$")
                                                       (body (list "alias /srv/wkd/$1;"
                                                                   "default_type application/octet-stream;"
                                                                   "add_header Access-Control-Allow-Origin * always;"))))))
                                    (nginx-server-configuration
                                     (server-name '("hyperlisp.net"))
                                     (listen '("443 ssl"))
                                     (root "/srv/http/hyperlisp.net")
                                     (ssl-certificate "/etc/letsencrypt/live/hyperlisp.net/fullchain.pem")
                                     (ssl-certificate-key "/etc/letsencrypt/live/hyperlisp.net/privkey.pem"))))))
                   (service certbot-service-type
                            (certbot-configuration
                             (email "shcv@sdf.org")
                             (certificates
                              (list
                               (certificate-configuration
                                (domains '("hackandstash.com"
                                           "www.hackandstash.com"))
                                (deploy-hook %nginx-deploy-hook))
                               (certificate-configuration
                                (domains '("hnst.sh" "www.hnst.sh" "smtp.hnst.sh" "mail.hnst.sh" "imap.hnst.sh"))
                                (deploy-hook %nginx-deploy-hook))
                               (certificate-configuration
                                (name "wkd")
                                (domains '("openpgpkey.hnst.sh" "openpgpkey.hyperlisp.net" "openpgpkey.hackandstash.com"))
                                (deploy-hook %nginx-deploy-hook))
                               (certificate-configuration
                                (domains '("hyperlisp.net"
                                           "www.hyperlisp.net"))
                                (deploy-hook %nginx-deploy-hook))))))
                   (service mumble-server-service-type
                            (mumble-server-configuration
                             (welcome-text
                              "Welcome to this Mumble server running on Guix!")
                             (cert-required? #t) ;disallow text password logins
                             (ssl-cert hnst-cert)
                             (ssl-key hnst-key)))
                   (service wireguard-service-type
                            (wireguard-configuration
                             (interface "wg0")
                             (addresses '("10.42.0.1/32"))
                             (post-up '("iptables -I FORWARD -i wg0 -o wg0 -j ACCEPT"))
                             (post-down '("iptables -D FORWARD -i wg0 -o wg0 -j ACCEPT"))
                             (peers
                              (list
                               (wireguard-peer
                                (name "terminus")
                                (public-key "bW82LuJzevvUPmqIqkfgCeQzdOnS9Afkktb03BSWgC0=")
                                (allowed-ips '("10.42.0.2/32")))
                               (wireguard-peer
                                (name "aexon")
                                (public-key "n0XCZ1tiJ3feK+i4UwtMQ44SJjJkBkeh5v325xPzYg4=")
                                (allowed-ips '("10.42.0.3/32")))
                               (wireguard-peer
                                (name "bacon")
                                (public-key "bPYZ91kSyfaijIkbo2jNSM/MExQKRv4pLDzfyKEChD4=")
                                (allowed-ips '("10.42.0.4/32")))
                               (wireguard-peer
                                (name "sandbox")
                                (public-key "bbptpu1h3HGAWCKNxtrtjrOVa8uY4an2uo1wGPTEblI=")
                                (allowed-ips '("10.42.0.5/32")))
                               (wireguard-peer
                                (name "zenbook")
                                (public-key "LD7r9JfPaKg7lvSxjbJGtj1QSaC6PH6Jdw9Igw19QEE=")
                                (allowed-ips '("10.42.0.6/32")))))))
                   (service rspamd-service-type)
                   (service opensmtpd-service-type
                            (opensmtpd-configuration
                             (shepherd-requirement '(networking))
                             (config-file
                              (mixed-text-file "smtpd.conf" "
table credentials file:/etc/mail/credentials
table virtual-users file:" virtual-mail-users "
table domains file:" virtual-mail-domains "
table aliases file:" mail-aliases "

include \"/etc/mail/srs.conf\"

pki self cert \"" hnst-cert "\"
pki self key \"" hnst-key "\"

filter check_dyndns phase connect match rdns regex { '.*\\.dyn\\..*', '.*\\.dsl\\..*' } \
    disconnect \"550 no residential connections\"

filter check_rdns phase connect match !rdns \
    disconnect \"550 Reverse DNS required\"

filter check_fcrdns phase connect match !fcrdns \
    disconnect \"550 Forward-confirmed reverse DNS required\"

filter rspamd proc-exec \"" opensmtpd-filter-rspamd "/libexec/opensmtpd/filter-rspamd \"

filter dkimsign proc-exec \"" opensmtpd-filter-dkimsign "/libexec/opensmtpd/filter-dkimsign -d hnst.sh -s 202401 -k /etc/mail/dkim/202401._domainkey.key\" user _dkimsign group _dkimsign

listen on eth0 port 25        hostname mail.hnst.sh pki self tls \
    filter { check_dyndns, check_rdns, check_fcrdns, rspamd }

listen on lo port 25          tag ORIGINATED_HERE hostname mail.hnst.sh mask-src filter dkimsign
listen on eth0 port 587       tag ORIGINATED_HERE hostname mail.hnst.sh filter dkimsign auth <credentials> pki self tls-require
listen on eth0 port 465       tag ORIGINATED_HERE hostname mail.hnst.sh mask-src filter dkimsign auth <credentials> pki self smtps
listen on socket              tag ORIGINATED_HERE mask-src filter dkimsign

action recv-remote maildir \"/home/%{user.username}/Mail\" junk virtual <virtual-users>
action recv-local maildir \"/home/%{user.username}/Mail\" junk alias <aliases>
action send relay srs helo mail.hnst.sh

match from any for domain <domains> action recv-remote
match from local for local action recv-local
match tag ORIGINATED_HERE from any for any action send
match from local for any action send
"))))

                   (service dovecot-service-type
                            (dovecot-configuration
                             (mail-location "maildir:~/Mail:LAYOUT=fs")
                             (listen '("*" "::"))
                             (ssl? "required")
                             (ssl-cert "</etc/letsencrypt/live/hnst.sh/fullchain.pem")
                             (ssl-key "</etc/letsencrypt/live/hnst.sh/privkey.pem")
                             (passdbs (list
                                       (passdb-configuration
                                        (driver "passwd-file")
                                        (args (list "username_format=%u" "/etc/mail/passwd")))))
                             (userdbs (list
                                       (userdb-configuration
                                        (driver "passwd-file")
                                        (args (list "username_format=%u" "/etc/mail/passwd")))))
                             (protocols (list
                                         (protocol-configuration
                                          (name "imap"))))
                             (services (list
                                        (service-configuration
                                         (kind "imap"))))))
                   %base-services)))
