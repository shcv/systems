#!env sh
mount /dev/sda2 /mnt
mount /dev/sda1 /mnt/boot/efi
mount --bind /proc /mnt/proc
mount --bind /sys /mnt/sys
mount --bind /dev /mnt/dev
chroot /mnt /bin/sh

