;;(use-modules (oop goops))
;;(add-to-load-path "/root/.guix-profile/share/guile/site/3.0/")
;;(use-modules (ice-9 readline))
;;(activate-readline)

(use-modules
 (gnu)
 (gnu services)
 (gnu services shepherd)
 (gnu services mcron)
 (gnu system linux-container)
 (gnu system images wsl2)
 (guix profiles)
 (guix packages)
 (srfi srfi-1))

(use-service-modules base networking ssh desktop xorg dbus docker mcron)
(use-package-modules emacs shells xorg fonts certs ssh bash base linux mail)

(define %font-list
  (list
   ;; lots of fonts from package fonts.scm
   font-adobe-source-code-pro font-adobe-source-han-sans
   font-adobe-source-sans-pro font-adobe-source-serif-pro
   font-anonymous-pro font-anonymous-pro-minus font-awesome
   font-bitstream-vera font-blackfoundry-inria font-abattis-cantarell
   font-cns11643 font-cns11643-swjz font-comic-neue font-culmus
   font-dejavu font-dosis font-dseg font-fantasque-sans font-fira-code
   font-fira-mono font-fira-sans font-fontna-yasashisa-antique
   font-gnu-freefont font-gnu-freefont font-gnu-unifont font-go
   font-google-material-design-icons font-google-noto font-google-roboto
   font-hack font-hermit font-ibm-plex font-inconsolata font-iosevka
   font-iosevka-aile font-iosevka-etoile font-iosevka-slab
   font-iosevka-term font-iosevka-term-slab
   font-ipa-mj-mincho font-jetbrains-mono font-lato font-liberation
   font-linuxlibertine font-lohit font-meera-inimai font-mononoki
   font-mplus-testflight font-opendyslexic
   font-public-sans font-rachana font-sarasa-gothic font-sil-andika
   font-sil-charis font-sil-gentium font-tamzen font-terminus
   font-tex-gyre font-un font-vazir font-wqy-microhei
   font-wqy-zenhei
   ;; lots of fonts from package xorg.scm
   font-adobe100dpi font-adobe75dpi font-cronyx-cyrillic font-dec-misc
   font-isas-misc font-micro-misc font-misc-cyrillic font-misc-ethiopic
   font-misc-misc font-mutt-misc font-schumacher-misc
   font-screen-cyrillic font-sony-misc font-sun-misc font-util
   font-winitzki-cyrillic font-xfree86-type1))

(define mailsync-job
  ;; Synchronize mail every five minutes
  ;; Runs as 'shcv' in /home/shcv
  #~(job "0 * * * *"
         (string-append #$isync "/bin/mbsync -a")
         #:user "shcv"))

(define (dummy-service _)
  (list
   (shepherd-service
    (documentation "")
    (provision '(loopback networking))
    (start #~(const #t))
    (stop #~(const #t))
    (respawn? #f))))

(define dummy-service-type
  (service-type
   (name 'loopback)
   (extensions
    (list (service-extension shepherd-root-service-type
                             dummy-service)))
   (default-value '())
   (description "")))

(define os
  (operating-system
   (inherit wsl-os)
   (host-name "aexon")
   (timezone "US/Eastern")
   (locale "en_US.utf8")
   (keyboard-layout (keyboard-layout "us" "colemak" #:options '("ctrl:nocaps")))

   (bootloader
    (bootloader-configuration
     (bootloader
      (bootloader
       (name 'dummybootloader)
       (package hello)
       (configuration-file "/dev/null")
       (configuration-file-generator (lambda* (. rest) (computed-file "dummybootloader" #~(mkdir #$output))))
       (installer #~(const #t))))))

   (file-systems (list (file-system
                        (device "/dev/sdb")
                        (mount-point "/")
                        (type "ext4")
                        (mount? #t))    ; saying #f here doesn't work :(
                       (file-system
                        (device "cgroup")
                        (mount-point "/sys/fs/cgroup/rdma")
                        (type "cgroup")
                        (check? #f)
                        (options "rdma")
                        (create-mount-point? #t)
                        (mount? #t))))

   (users (cons (user-account
                 (name "shcv")
                 (home-directory "/home/shcv")
                 (uid 1000)
                 (group "users")
                 (supplementary-groups '("wheel" "docker"))
                 (shell (file-append zsh "/bin/zsh")))
                %base-user-accounts))

   (packages (append (list              ; global packages to add
                      nss-certs
                      openssh
                      glibc-utf8-locales)
                     %font-list
                     (remove
                      (lambda (x)
                        (member (package-name x)
                                (list   ; global packages to not add
                                 "nano"
                                 "info-reader"
                                 "pciutils"
                                 "usbutils"
                                 "util-linux-with-udev"
                                 "kmod"
                                 "eudev"
                                 "isc-dhcp"
                                 "iw"
                                 "wireless-tools")))
                      %base-packages)))

   (services (cons*
              (service dummy-service-type)
              (syslog-service)
              (service urandom-seed-service-type)

              ;; necessary for docker
              (service elogind-service-type)
              (service dbus-root-service-type)
              (service docker-service-type)
                   
              (set-xorg-configuration
               (xorg-configuration
                (keyboard-layout keyboard-layout)))
              (simple-service 'my-cron-jobs
                              mcron-service-type
                              (list mailsync-job))
              (operating-system-user-services wsl-os)))))

os
